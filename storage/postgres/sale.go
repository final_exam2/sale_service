package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/final_exam2/sale_service/genproto/sale_service"
	"gitlab.com/final_exam2/sale_service/pkg/helper"
)

type SaleRepo struct {
	db *pgxpool.Pool
}

func NewSaleRepo(db *pgxpool.Pool) *SaleRepo {
	return &SaleRepo{
		db: db,
	}
}

func (r *SaleRepo) Create(ctx context.Context, req *sale_service.SaleCreate) (*sale_service.Sale, error) {

	count, err := r.GetList(ctx, &sale_service.SaleGetListRequest{})
	if err != nil {
		return nil, err
	}

	var (
		id      = uuid.New().String()
		query   string
		sale_id = helper.GenerateString(string(req.BranchId[0]), int(count.Count)+1)
	)

	query = `
		INSERT INTO sales(id, branch_id, shift_id, market_id , staff_id,payment_id,sale_id, updated_at)
		VALUES ($1, $2, $3 , $4, $5, $6, $7,NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		req.BranchId,
		req.ShiftId,
		req.MarketId,
		req.StaffId,
		req.PaymentId,
		sale_id,
	)

	if err != nil {
		return nil, err
	}

	return &sale_service.Sale{
		Id:        id,
		BranchId:  req.BranchId,
		ShiftId:   req.ShiftId,
		MarketId:  req.MarketId,
		StaffId:   req.StaffId,
		Status:    "in proccess",
		PaymentId: req.PaymentId,
		SaleId:    sale_id,
	}, nil
}

func (r *SaleRepo) GetByID(ctx context.Context, req *sale_service.SalePrimaryKey) (*sale_service.Sale, error) {

	var (
		query string

		id         sql.NullString
		branch_id  sql.NullString
		shift_id   sql.NullString
		market_id  sql.NullString
		staff_id   sql.NullString
		status     sql.NullString
		payment_id sql.NullString
		sale_id    sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	query = `
		SELECT
			id,
			branch_id,
			shift_id,
			market_id,
			staff_id,
			status,
			payment_id,
			sale_id,
			created_at,
			updated_at		
		FROM sales
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branch_id,
		&shift_id,
		&market_id,
		&staff_id,
		&status,
		&payment_id,
		&sale_id,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &sale_service.Sale{
		Id:        id.String,
		BranchId:  branch_id.String,
		ShiftId:   shift_id.String,
		MarketId:  market_id.String,
		StaffId:   staff_id.String,
		Status:    status.String,
		PaymentId: payment_id.String,
		SaleId:    sale_id.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}, nil
}

func (r *SaleRepo) GetList(ctx context.Context, req *sale_service.SaleGetListRequest) (*sale_service.SaleGetListResponse, error) {

	var (
		resp   = &sale_service.SaleGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			shift_id,
			market_id,
			staff_id,
			status,
			payment_id,
			sale_id,
			created_at,
			updated_at	
		FROM sales
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchShiftId != "" {
		where += ` AND shift_id ILIKE '%' || '` + req.SearchShiftId + `' || '%'`
	}

	if req.SearchStaff != "" {
		where += ` AND shift_id ILIKE '%' || '` + req.SearchStaff + `' || '%'`
	}

	if req.SearchBybranch != "" {
		where += ` AND branch_id ILIKE '%' || '` + req.SearchBybranch + `' || '%'`
	}

	if req.SearchBySaleId != "" {
		where += ` AND sale_id ILIKE '%' || '` + req.SearchBySaleId + `' || '%'`
	}
	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			branch_id  sql.NullString
			shift_id   sql.NullString
			market_id  sql.NullString
			staff_id   sql.NullString
			status     sql.NullString
			payment_id sql.NullString
			sale_id    sql.NullString
			created_at sql.NullString
			updated_at sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branch_id,
			&shift_id,
			&market_id,
			&staff_id,
			&status,
			&payment_id,
			&sale_id,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Sales = append(resp.Sales, &sale_service.Sale{
			Id:        id.String,
			BranchId:  branch_id.String,
			ShiftId:   shift_id.String,
			MarketId:  market_id.String,
			StaffId:   staff_id.String,
			Status:    status.String,
			PaymentId: payment_id.String,
			SaleId:    sale_id.String,
			CreatedAt: created_at.String,
			UpdatedAt: updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *SaleRepo) Update(ctx context.Context, req *sale_service.SaleUpdate) (*sale_service.Sale, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
		sales
		SET
		branch_id = :branch_id,
		shift_id = :shift_id,
		market_id = :market_id,
		staff_id = :staff_id,
		status = :status,
		payment_id = :payment_id,
		sale_id = :sale_id,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":         req.GetId(),
		"branch_id":  req.GetBranchId(),
		"shift_id":   req.GetShiftId(),
		"market_id":  req.GetMarketId(),
		"staff_id":   req.GetStaffId(),
		"status":     req.GetStatus(),
		"payment_id": req.GetPaymentId(),
		"sale_id":    req.GetSaleId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &sale_service.Sale{
		Id:        req.Id,
		BranchId:  req.BranchId,
		ShiftId:   req.ShiftId,
		MarketId:  req.MarketId,
		StaffId:   req.StaffId,
		Status:    req.Status,
		PaymentId: req.PaymentId,
		SaleId:    req.SaleId,
	}, nil
}

func (r *SaleRepo) Delete(ctx context.Context, req *sale_service.SalePrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM sales WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
