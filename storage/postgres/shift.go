package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/final_exam2/sale_service/genproto/sale_service"
	"gitlab.com/final_exam2/sale_service/pkg/helper"
)

type ShiftRepo struct {
	db *pgxpool.Pool
}

func NewShiftRepo(db *pgxpool.Pool) *ShiftRepo {
	return &ShiftRepo{
		db: db,
	}
}

func (r *ShiftRepo) Create(ctx context.Context, req *sale_service.ShiftCreate) (*sale_service.Shift, error) {
	count, err := r.GetList(ctx, &sale_service.ShiftGetListRequest{})
	if err != nil {
		return nil, err
	}

	var (
		id             = uuid.New().String()
		query          string
		transaction_id = uuid.New().String()
		shift_id       = helper.GenerateString("Sh", int(count.Count)+1)
	)

	query = `
		INSERT INTO shift(id, branch_id,staff_id, provider_id, market_id,transaction_id,shift_id, updated_at)
		VALUES ($1, $2, $3 , $4,$5, $6,$7, NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		req.BranchId,
		req.StaffId,
		req.ProviderId,
		req.MarketId,
		transaction_id,
		shift_id,
	)

	if err != nil {
		return nil, err
	}

	return &sale_service.Shift{
		Id:            id,
		BranchId:      req.BranchId,
		StaffId:       req.StaffId,
		ProviderId:    req.ProviderId,
		MarketId:      req.MarketId,
		TransactionId: transaction_id,
		ShiftId:       shift_id,
	}, nil
}

func (r *ShiftRepo) GetByID(ctx context.Context, req *sale_service.ShiftPrimaryKey) (*sale_service.Shift, error) {

	var (
		query string

		id             sql.NullString
		branch_id      sql.NullString
		staff_id       sql.NullString
		provider_id    sql.NullString
		market_id      sql.NullString
		status         sql.NullString
		transaction_id sql.NullString
		shift_id       sql.NullString
		created_at     sql.NullString
		updated_at     sql.NullString
	)

	query = `
		SELECT
			id,
			branch_id,
			staff_id,
			provider_id,
			market_id,
			status,
			transaction_id,
			shift_id,
			created_at,
			updated_at		
		FROM shift
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branch_id,
		&staff_id,
		&provider_id,
		&market_id,
		&status,
		&transaction_id,
		&shift_id,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &sale_service.Shift{
		Id:            id.String,
		BranchId:      branch_id.String,
		StaffId:       staff_id.String,
		ProviderId:    provider_id.String,
		MarketId:      market_id.String,
		Status:        status.String,
		TransactionId: transaction_id.String,
		ShiftId:       shift_id.String,
		CreatedAt:     created_at.String,
		UpdatedAt:     updated_at.String,
	}, nil
}

func (r *ShiftRepo) GetList(ctx context.Context, req *sale_service.ShiftGetListRequest) (*sale_service.ShiftGetListResponse, error) {

	var (
		resp   = &sale_service.ShiftGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			staff_id,
			provider_id,
			market_id,
			status,
			transaction_id,
			shift_id,
			created_at,
			updated_at	
		FROM shift
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByShiftId != "" {
		where += ` AND shift_id ILIKE '%' || '` + req.SearchByShiftId + `' || '%'`
	}

	if req.SearchByStaff != "" {
		where += ` AND staff_id ILIKE '%' || '` + req.SearchByStaff + `' || '%'`
	}

	if req.SearchByBranch != "" {
		where += ` AND branch_id ILIKE '%' || '` + req.SearchByBranch + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id             sql.NullString
			branch_id      sql.NullString
			staff_id       sql.NullString
			provider_id    sql.NullString
			market_id      sql.NullString
			status         sql.NullString
			shift_id       sql.NullString
			transaction_id sql.NullString
			created_at     sql.NullString
			updated_at     sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branch_id,
			&staff_id,
			&provider_id,
			&market_id,
			&status,
			&transaction_id,
			&shift_id,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Shifts = append(resp.Shifts, &sale_service.Shift{
			Id:            id.String,
			BranchId:      branch_id.String,
			StaffId:       staff_id.String,
			ProviderId:    provider_id.String,
			MarketId:      market_id.String,
			Status:        status.String,
			TransactionId: transaction_id.String,
			ShiftId:       shift_id.String,
			CreatedAt:     created_at.String,
			UpdatedAt:     updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *ShiftRepo) Update(ctx context.Context, req *sale_service.ShiftUpdate) (*sale_service.Shift, error) {

	var (
		query          string
		params         map[string]interface{}
		transaction_id = uuid.New().String()
	)

	query = `
		UPDATE
		shift
		SET
		branch_id = :branch_id,
		staff_id = :staff_id,
		provider_id = :provider_id,
		market_id = :market_id,
		transaction_id = :transaction_id,
		status = :status,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":             req.GetId(),
		"branch_id":      req.GetBranchId(),
		"staff_id":       req.GetStaffId(),
		"provider_id":    req.GetProviderId(),
		"market_id":      req.GetMarketId(),
		"status":         req.GetStatus(),
		"transaction_id": req.GetTransactionId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &sale_service.Shift{
		Id:            req.Id,
		BranchId:      req.BranchId,
		StaffId:       req.StaffId,
		ProviderId:    req.ProviderId,
		MarketId:      req.MarketId,
		TransactionId: transaction_id,
	}, nil
}

func (r *ShiftRepo) Delete(ctx context.Context, req *sale_service.ShiftPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM shift WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
