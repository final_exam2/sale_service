package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/final_exam2/sale_service/genproto/sale_service"
	"gitlab.com/final_exam2/sale_service/pkg/helper"
)

type TransactionRepo struct {
	db *pgxpool.Pool
}

func NewTransactionRepo(db *pgxpool.Pool) *TransactionRepo {
	return &TransactionRepo{
		db: db,
	}
}

func (r *TransactionRepo) Create(ctx context.Context, req *sale_service.TransactionCreate) (*sale_service.Transaction, error) {

	var (
		id          = uuid.New().String()
		query       string
		total_price float64 = 0
	)

	query = `
		INSERT INTO transaction(id, cash,uzcard, payme, click ,humo,apelsin,total_price, updated_at)
		VALUES ($1, $2, $3 , $4,$5, $6, $7,$8, NOW())
	`
	total_price = req.Cash + req.Uzcard + req.Payme + req.Click + req.Humo + req.Apelsin
	_, err := r.db.Exec(ctx, query,
		id,
		req.Cash,
		req.Uzcard,
		req.Payme,
		req.Click,
		req.Humo,
		req.Apelsin,
		total_price,
	)

	if err != nil {
		return nil, err
	}

	return &sale_service.Transaction{
		Id:         id,
		Cash:       req.Cash,
		Uzcard:     req.Uzcard,
		Payme:      req.Payme,
		Click:      req.Click,
		Humo:       req.Humo,
		Apelsin:    req.Apelsin,
		TotalPrice: total_price,
	}, nil
}

func (r *TransactionRepo) GetByID(ctx context.Context, req *sale_service.TransactionPrimaryKey) (*sale_service.Transaction, error) {

	var (
		query string

		id          sql.NullString
		cash        float64
		uzcard      float64
		payme       float64
		click       float64
		humo        float64
		apelsin     float64
		total_price float64
		created_at  sql.NullString
		updated_at  sql.NullString
	)

	query = `
		SELECT
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			apelsin,
			total_price,
			created_at,
			updated_at		
		FROM transaction
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&cash,
		&uzcard,
		&payme,
		&click,
		&humo,
		&apelsin,
		&total_price,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &sale_service.Transaction{
		Id:         id.String,
		Cash:       cash,
		Uzcard:     uzcard,
		Payme:      payme,
		Click:      click,
		Humo:       humo,
		Apelsin:    apelsin,
		TotalPrice: total_price,
		CreatedAt:  created_at.String,
		UpdatedAt:  updated_at.String,
	}, nil
}

func (r *TransactionRepo) GetList(ctx context.Context, req *sale_service.TransactionGetListRequest) (*sale_service.TransactionGetListResponse, error) {

	var (
		resp   = &sale_service.TransactionGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			apelsin,
			total_price,
			created_at,
			updated_at		
		FROM transaction
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND bar_code ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			cash        float64
			uzcard      float64
			payme       float64
			click       float64
			humo        float64
			apelsin     float64
			total_price float64
			created_at  sql.NullString
			updated_at  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&cash,
			&uzcard,
			&payme,
			&click,
			&humo,
			&apelsin,
			&total_price,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Transactions = append(resp.Transactions, &sale_service.Transaction{
			Id:         id.String,
			Cash:       cash,
			Uzcard:     uzcard,
			Payme:      payme,
			Click:      click,
			Humo:       humo,
			Apelsin:    apelsin,
			TotalPrice: total_price,
			CreatedAt:  created_at.String,
			UpdatedAt:  updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *TransactionRepo) Update(ctx context.Context, req *sale_service.TransactionUpdate) (*sale_service.Transaction, error) {

	var (
		query       string
		params      map[string]interface{}
		total_price float64 = 0
	)

	query = `
		UPDATE
		transaction
		SET
		cash =:cash,
		uzcard =:uzcard,
		payme =:payme,
		click =:click,
		humo =:humo,
		apelsin =:apelsin,
		total_price =:total_price,
			updated_at = NOW()
		WHERE id = :id
	`
	total_price = req.Cash + req.Uzcard + req.Payme + req.Humo + req.Apelsin

	params = map[string]interface{}{
		"id":          req.GetId(),
		"cash":        req.GetCash(),
		"uzcard":      req.GetUzcard(),
		"payme":       req.GetPayme(),
		"click":       req.GetClick(),
		"humo":        req.GetHumo(),
		"apelsin":     req.GetApelsin(),
		"total_price": total_price,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &sale_service.Transaction{
		Id:         req.Id,
		Cash:       req.Cash,
		Uzcard:     req.Uzcard,
		Payme:      req.Payme,
		Click:      req.Click,
		Humo:       req.Humo,
		Apelsin:    req.Apelsin,
		TotalPrice: total_price,
	}, nil
}

func (r *TransactionRepo) Delete(ctx context.Context, req *sale_service.TransactionPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM transaction WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
