package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/final_exam2/sale_service/genproto/sale_service"
	"gitlab.com/final_exam2/sale_service/pkg/helper"
)

type PaymentRepo struct {
	db *pgxpool.Pool
}

func NewPaymentRepo(db *pgxpool.Pool) *PaymentRepo {
	return &PaymentRepo{
		db: db,
	}
}

func (r *PaymentRepo) Create(ctx context.Context, req *sale_service.PaymentCreate) (*sale_service.Payment, error) {

	var (
		id          = uuid.New().String()
		query       string
		total_price = req.Cash + req.Uzcard + req.Payme + req.Humo + req.Apelsin + req.Visa
	)

	query = `
		INSERT INTO payment(id, cash,uzcard, payme, click ,humo,apelsin,visa,currency,exchange_sum,total_price, updated_at)
		VALUES ($1, $2, $3 , $4,$5, $6, $7,$8,$9,$10,$11, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Cash,
		req.Uzcard,
		req.Payme,
		req.Click,
		req.Humo,
		req.Apelsin,
		req.Visa,
		req.Currency,
		req.ExchangeSum,
		total_price,
	)

	if err != nil {
		return nil, err
	}

	return &sale_service.Payment{
		Id:          id,
		Cash:        req.Cash,
		Uzcard:      req.Uzcard,
		Payme:       req.Payme,
		Click:       req.Click,
		Humo:        req.Humo,
		Apelsin:     req.Apelsin,
		Visa:        req.Visa,
		Currency:    req.Currency,
		ExchangeSum: req.ExchangeSum,
		TotalPrice:  total_price,
	}, nil
}

func (r *PaymentRepo) GetByID(ctx context.Context, req *sale_service.PaymentPrimaryKey) (*sale_service.Payment, error) {

	var (
		query string

		id           sql.NullString
		cash         sql.NullFloat64
		uzcard       sql.NullFloat64
		payme        sql.NullFloat64
		click        sql.NullFloat64
		humo         sql.NullFloat64
		apelsin      sql.NullFloat64
		visa         sql.NullFloat64
		currency     sql.NullString
		exchange_sum sql.NullFloat64
		total_price  sql.NullFloat64
		created_at   sql.NullString
		updated_at   sql.NullString
	)

	query = `
		SELECT
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			apelsin,
			visa,
			currency,
			exchange_sum,
			total_price,
			created_at,
			updated_at		
		FROM payment
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&cash,
		&uzcard,
		&payme,
		&click,
		&humo,
		&apelsin,
		&visa,
		&currency,
		&exchange_sum,
		&total_price,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &sale_service.Payment{
		Id:          id.String,
		Cash:        cash.Float64,
		Uzcard:      uzcard.Float64,
		Payme:       payme.Float64,
		Click:       click.Float64,
		Humo:        humo.Float64,
		Apelsin:     apelsin.Float64,
		Visa:        visa.Float64,
		Currency:    currency.String,
		ExchangeSum: exchange_sum.Float64,
		TotalPrice:  total_price.Float64,
		CreatedAt:   created_at.String,
		UpdatedAt:   updated_at.String,
	}, nil
}

func (r *PaymentRepo) GetList(ctx context.Context, req *sale_service.PaymentGetListRequest) (*sale_service.PaymentGetListResponse, error) {

	var (
		resp   = &sale_service.PaymentGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			apelsin,
			visa,
			currency,
			exchange_sum,
			total_price,
			created_at,
			updated_at	
		FROM payment
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND bar_code ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			cash         float64
			uzcard       float64
			payme        float64
			click        float64
			humo         float64
			apelsin      float64
			visa         float64
			currency     sql.NullString
			exchange_sum float64
			total_price  float64
			created_at   sql.NullString
			updated_at   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&cash,
			&uzcard,
			&payme,
			&click,
			&humo,
			&apelsin,
			&visa,
			&currency,
			&exchange_sum,
			&total_price,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Payments = append(resp.Payments, &sale_service.Payment{
			Id:          id.String,
			Cash:        cash,
			Uzcard:      uzcard,
			Payme:       payme,
			Click:       click,
			Humo:        humo,
			Apelsin:     apelsin,
			Visa:        visa,
			Currency:    currency.String,
			ExchangeSum: exchange_sum,
			TotalPrice:  total_price,
			CreatedAt:   created_at.String,
			UpdatedAt:   updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *PaymentRepo) Update(ctx context.Context, req *sale_service.PaymentUpdate) (*sale_service.Payment, error) {

	var (
		query       string
		params      map[string]interface{}
		total_price = req.Cash + req.Uzcard + req.Payme + req.Humo + req.Apelsin + req.Visa
	)

	query = `
		UPDATE
		payment
		SET
		cash = :cash,
		uzcard = :uzcard,
		payme =: payme,
		click = :click,
		humo = :humo,
		apelsin = :apelsin,
		visa = :visa,
		currency = :currency,
		exchange_sum = :exchange_sum,
		total_price = :total_price,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.GetId(),
		"cash":         req.GetCash(),
		"uzcard":       req.GetUzcard(),
		"payme":        req.GetPayme(),
		"click":        req.GetClick(),
		"humo":         req.GetHumo(),
		"apelsin":      req.GetApelsin(),
		"visa":         req.GetVisa(),
		"currency":     req.GetCurrency(),
		"exchange_sum": req.GetExchangeSum(),
		"total_price":  total_price,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &sale_service.Payment{
		Id:          req.Id,
		Cash:        req.Cash,
		Uzcard:      req.Uzcard,
		Payme:       req.Payme,
		Click:       req.Click,
		Humo:        req.Humo,
		Apelsin:     req.Apelsin,
		Visa:        req.Visa,
		Currency:    req.Currency,
		ExchangeSum: req.ExchangeSum,
		TotalPrice:  total_price,
	}, nil
}

func (r *PaymentRepo) Delete(ctx context.Context, req *sale_service.PaymentPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM payment WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
