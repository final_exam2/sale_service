package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/final_exam2/sale_service/genproto/sale_service"
	"gitlab.com/final_exam2/sale_service/pkg/helper"
)

type SaleProductRepo struct {
	db *pgxpool.Pool
}

func NewSaleProductRepo(db *pgxpool.Pool) *SaleProductRepo {
	return &SaleProductRepo{
		db: db,
	}
}

func (r *SaleProductRepo) Create(ctx context.Context, req *sale_service.SaleProductCreate) (*sale_service.SaleProduct, error) {

	var (
		id    = uuid.New().String()
		query string
	)
	total_price := req.Count * int64(req.Price)

	query = `
		INSERT INTO sale_product(id, brand_id, product_id, category_id , bar_code,remainder_id,count,price,total_price,sale_id, updated_at)
		VALUES ($1, $2, $3 , $4,$5, $6, $7, $8 , $9,$10,NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.BrandId,
		req.ProductId,
		req.CategoryId,
		req.BarCode,
		req.RemainderId,
		req.Count,
		req.Price,
		total_price,
		req.SaleId,
	)

	if err != nil {
		return nil, err
	}

	return &sale_service.SaleProduct{
		Id:          id,
		BrandId:     req.BrandId,
		ProductId:   req.ProductId,
		CategoryId:  req.CategoryId,
		BarCode:     req.BarCode,
		RemainderId: req.RemainderId,
		Count:       req.Count,
		Price:       req.Price,
		TotalPrice:  float64(total_price),
		SaleId:      req.SaleId,
	}, nil
}

func (r *SaleProductRepo) GetByID(ctx context.Context, req *sale_service.SaleProductPrimaryKey) (*sale_service.SaleProduct, error) {

	var (
		query string

		id           sql.NullString
		brand_id     sql.NullString
		product_id   sql.NullString
		category_id  sql.NullString
		bar_code     sql.NullString
		remainder_id sql.NullString
		count        int64
		price        float64
		total_price  sql.NullFloat64
		sale_id      sql.NullString
		created_at   sql.NullString
		updated_at   sql.NullString
	)

	query = `
		SELECT
			id,
			brand_id,
			product_id,
			category_id,
			bar_code,
			remainder_id,
			count,
			price,
			total_price,
			sale_id,
			created_at,
			updated_at		
		FROM sale_product
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&brand_id,
		&product_id,
		&category_id,
		&bar_code,
		&remainder_id,
		&count,
		&price,
		&total_price,
		&sale_id,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &sale_service.SaleProduct{
		Id:          id.String,
		BrandId:     brand_id.String,
		CategoryId:  category_id.String,
		ProductId:   product_id.String,
		BarCode:     bar_code.String,
		RemainderId: remainder_id.String,
		Count:       count,
		Price:       price,
		TotalPrice:  total_price.Float64,
		SaleId:      sale_id.String,
		CreatedAt:   created_at.String,
		UpdatedAt:   updated_at.String,
	}, nil
}

func (r *SaleProductRepo) GetList(ctx context.Context, req *sale_service.SaleProductGetListRequest) (*sale_service.SaleProductGetListResponse, error) {

	var (
		resp   = &sale_service.SaleProductGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			brand_id,
			product_id,
			category_id,
			bar_code,
			remainder_id,
			count,
			price,
			total_price,
			sale_id,
			created_at,
			updated_at	
		FROM sale_product
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByBrand != "" {
		where += ` AND brand_id ILIKE '%' || '` + req.SearchByBrand + `' || '%'`
	}

	if req.SearchByCategory != "" {
		where += ` AND category_id ILIKE '%' || '` + req.SearchByCategory + `' || '%'`
	}

	if req.SearchByBarCode != "" {
		where += ` AND bar_code ILIKE '%' || '` + req.SearchByBarCode + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			brand_id     sql.NullString
			product_id   sql.NullString
			category_id  sql.NullString
			bar_code     sql.NullString
			remainder_id sql.NullString
			count        sql.NullInt64
			price        sql.NullFloat64
			total_price  sql.NullFloat64
			sale_id      sql.NullString
			created_at   sql.NullString
			updated_at   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&brand_id,
			&product_id,
			&category_id,
			&bar_code,
			&remainder_id,
			&count,
			&price,
			&total_price,
			&sale_id,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.SaleProducts = append(resp.SaleProducts, &sale_service.SaleProduct{
			Id:          id.String,
			BrandId:     brand_id.String,
			CategoryId:  category_id.String,
			ProductId:   product_id.String,
			BarCode:     bar_code.String,
			RemainderId: remainder_id.String,
			Count:       count.Int64,
			Price:       price.Float64,
			TotalPrice:  total_price.Float64,
			SaleId:      sale_id.String,
			CreatedAt:   created_at.String,
			UpdatedAt:   updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *SaleProductRepo) Update(ctx context.Context, req *sale_service.SaleProductUpdate) (*sale_service.SaleProduct, error) {

	var (
		query  string
		params map[string]interface{}
	)
	total_price := req.Count * int64(req.Price)

	query = `
		UPDATE
		sale_product
		SET
			brand_id = :brand_id,
			category_id = :category_id,
			product_id =: product_id,
			bar_code = :bar_code,
			remainder_id = :remainder_id,
			count = :count,
			price = :price,
			total_price = :total_price,
			sale_id =:sale_id,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.GetId(),
		"brand_id":     req.GetBrandId(),
		"category_id":  req.GetCategoryId(),
		"product_id":   req.GetProductId(),
		"bar_code":     req.GetBarCode(),
		"remainder_id": req.GetRemainderId(),
		"count":        req.GetCount(),
		"price":        req.GetPrice(),
		"total_price":  total_price,
		"sale_id":      req.GetSaleId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &sale_service.SaleProduct{
		Id:          req.Id,
		BrandId:     req.BrandId,
		CategoryId:  req.CategoryId,
		ProductId:   req.ProductId,
		BarCode:     req.BarCode,
		RemainderId: req.RemainderId,
		Count:       req.Count,
		Price:       req.Price,
		TotalPrice:  float64(total_price),
		SaleId:      req.SaleId,
	}, nil
}

func (r *SaleProductRepo) Delete(ctx context.Context, req *sale_service.SaleProductPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM sale_product WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
