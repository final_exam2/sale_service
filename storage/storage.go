package storage

import (
	"context"

	"gitlab.com/final_exam2/sale_service/genproto/sale_service"
)

type StorageI interface {
	CloseDB()
	SaleProduct() SaleProductRepoI
	Sale() SaleRepoI
	Shift() ShiftRepoI
	Payment() PaymentRepoI
	Transaction() TransactionRepoI
}

type SaleProductRepoI interface {
	Create(context.Context, *sale_service.SaleProductCreate) (*sale_service.SaleProduct, error)
	GetByID(context.Context, *sale_service.SaleProductPrimaryKey) (*sale_service.SaleProduct, error)
	GetList(context.Context, *sale_service.SaleProductGetListRequest) (*sale_service.SaleProductGetListResponse, error)
	Update(context.Context, *sale_service.SaleProductUpdate) (*sale_service.SaleProduct, error)
	Delete(context.Context, *sale_service.SaleProductPrimaryKey) error
}

type SaleRepoI interface {
	Create(context.Context, *sale_service.SaleCreate) (*sale_service.Sale, error)
	GetByID(context.Context, *sale_service.SalePrimaryKey) (*sale_service.Sale, error)
	GetList(context.Context, *sale_service.SaleGetListRequest) (*sale_service.SaleGetListResponse, error)
	Update(context.Context, *sale_service.SaleUpdate) (*sale_service.Sale, error)
	Delete(context.Context, *sale_service.SalePrimaryKey) error
}

type ShiftRepoI interface {
	Create(context.Context, *sale_service.ShiftCreate) (*sale_service.Shift, error)
	GetByID(context.Context, *sale_service.ShiftPrimaryKey) (*sale_service.Shift, error)
	GetList(context.Context, *sale_service.ShiftGetListRequest) (*sale_service.ShiftGetListResponse, error)
	Update(context.Context, *sale_service.ShiftUpdate) (*sale_service.Shift, error)
	Delete(context.Context, *sale_service.ShiftPrimaryKey) error
}

type PaymentRepoI interface {
	Create(context.Context, *sale_service.PaymentCreate) (*sale_service.Payment, error)
	GetByID(context.Context, *sale_service.PaymentPrimaryKey) (*sale_service.Payment, error)
	GetList(context.Context, *sale_service.PaymentGetListRequest) (*sale_service.PaymentGetListResponse, error)
	Update(context.Context, *sale_service.PaymentUpdate) (*sale_service.Payment, error)
	Delete(context.Context, *sale_service.PaymentPrimaryKey) error
}

type TransactionRepoI interface {
	Create(context.Context, *sale_service.TransactionCreate) (*sale_service.Transaction, error)
	GetByID(context.Context, *sale_service.TransactionPrimaryKey) (*sale_service.Transaction, error)
	GetList(context.Context, *sale_service.TransactionGetListRequest) (*sale_service.TransactionGetListResponse, error)
	Update(context.Context, *sale_service.TransactionUpdate) (*sale_service.Transaction, error)
	Delete(context.Context, *sale_service.TransactionPrimaryKey) error
}
