package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/final_exam2/sale_service/config"
	"gitlab.com/final_exam2/sale_service/genproto/sale_service"
	"gitlab.com/final_exam2/sale_service/grpc/client"
	"gitlab.com/final_exam2/sale_service/pkg/logger"
	"gitlab.com/final_exam2/sale_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type PaymentService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*sale_service.UnimplementedPaymentServiceServer
}

func NewPaymentService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *PaymentService {
	return &PaymentService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *PaymentService) Create(ctx context.Context, req *sale_service.PaymentCreate) (*sale_service.Payment, error) {
	u.log.Info("====== Payment Create ======", logger.Any("req", req))

	resp, err := u.strg.Payment().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Payment: u.strg.Payment().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *PaymentService) GetById(ctx context.Context, req *sale_service.PaymentPrimaryKey) (*sale_service.Payment, error) {
	u.log.Info("====== Payment Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Payment().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Payment Get By ID: u.strg.Payment().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *PaymentService) GetList(ctx context.Context, req *sale_service.PaymentGetListRequest) (*sale_service.PaymentGetListResponse, error) {
	u.log.Info("====== Payment Get List ======", logger.Any("req", req))

	resp, err := u.strg.Payment().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Payment Get List: u.strg.Payment().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *PaymentService) Update(ctx context.Context, req *sale_service.PaymentUpdate) (*sale_service.Payment, error) {
	u.log.Info("====== Product Update ======", logger.Any("req", req))

	resp, err := u.strg.Payment().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Payment Update: u.strg.Payment().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *PaymentService) Delete(ctx context.Context, req *sale_service.PaymentPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Payment Delete ======", logger.Any("req", req))

	err := u.strg.Payment().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Payment Delete: u.strg.Payment().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
