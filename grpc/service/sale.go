package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/final_exam2/sale_service/config"
	"gitlab.com/final_exam2/sale_service/genproto/sale_service"
	"gitlab.com/final_exam2/sale_service/grpc/client"
	"gitlab.com/final_exam2/sale_service/pkg/logger"
	"gitlab.com/final_exam2/sale_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type SaleService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*sale_service.UnimplementedSaleServiceServer
}

func NewSaleService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *SaleService {
	return &SaleService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *SaleService) Create(ctx context.Context, req *sale_service.SaleCreate) (resp *sale_service.Sale, err error) {
	u.log.Info("====== Sale Create ======", logger.Any("req", req))

	shift, err := u.strg.Shift().GetByID(ctx, &sale_service.ShiftPrimaryKey{Id: req.ShiftId})
	if err != nil {
		u.log.Error("Error While GetByID Shift: u.strg.Shift().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	if shift.Status == "opened" {
		resp, err = u.strg.Sale().Create(ctx, req)
		if err != nil {
			u.log.Error("Error While Create Sale: u.strg.Sale().Create", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
	} else {
		u.log.Error("You don't have an open shift, open a shift first", logger.Error(err))

	}


	return resp, nil
}

func (u *SaleService) GetById(ctx context.Context, req *sale_service.SalePrimaryKey) (*sale_service.Sale, error) {
	u.log.Info("====== Sale Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Sale().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Sale Get By ID: u.strg.Sale().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SaleService) GetList(ctx context.Context, req *sale_service.SaleGetListRequest) (*sale_service.SaleGetListResponse, error) {
	u.log.Info("====== Sale Get List ======", logger.Any("req", req))

	resp, err := u.strg.Sale().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Sale Get List: u.strg.Sale().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SaleService) Update(ctx context.Context, req *sale_service.SaleUpdate) (*sale_service.Sale, error) {
	u.log.Info("====== Sale Update ======", logger.Any("req", req))

	resp, err := u.strg.Sale().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Sale Update: u.strg.Sale().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SaleService) Delete(ctx context.Context, req *sale_service.SalePrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Sale Delete ======", logger.Any("req", req))

	err := u.strg.Sale().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Sale Delete: u.strg.Sale().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
