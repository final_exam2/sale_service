package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/final_exam2/sale_service/config"
	"gitlab.com/final_exam2/sale_service/genproto/sale_service"
	"gitlab.com/final_exam2/sale_service/grpc/client"
	"gitlab.com/final_exam2/sale_service/pkg/logger"
	"gitlab.com/final_exam2/sale_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type ShiftService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*sale_service.UnimplementedShiftServiceServer
}

func NewShiftService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ShiftService {
	return &ShiftService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *ShiftService) Create(ctx context.Context, req *sale_service.ShiftCreate) (resp *sale_service.Shift, err error) {
	u.log.Info("====== Shift Create ======", logger.Any("req", req))

	shifts, err := u.strg.Shift().GetList(ctx, &sale_service.ShiftGetListRequest{})
	if err != nil {
		u.log.Error("Error While GetList Shift: u.strg.Shift().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	statuss := false

	for _, val := range shifts.Shifts {
		if val.BranchId == req.BranchId {
			statuss = true
		}
	}
	if statuss == false {
		resp, err = u.strg.Shift().Create(ctx, req)
		if err != nil {
			u.log.Error("Error While Create Shift: u.strg.Shift().Create", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		_, err = u.strg.Shift().GetByID(ctx, &sale_service.ShiftPrimaryKey{Id: resp.Id})
		if err != nil {
			u.log.Error("Error While GetByID Shift: u.strg.Shift().GetByID", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		if err != nil {
			u.log.Error("Error While Update Shift: u.strg.Shift().Update", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

	} else {
		u.log.Error("This branch already has an open shift. Please close it first", logger.Error(err))

	}

	return resp, nil
}

func (u *ShiftService) GetById(ctx context.Context, req *sale_service.ShiftPrimaryKey) (*sale_service.Shift, error) {
	u.log.Info("====== Shift Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Shift().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Shift Get By ID: u.strg.Shift().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ShiftService) GetList(ctx context.Context, req *sale_service.ShiftGetListRequest) (*sale_service.ShiftGetListResponse, error) {
	u.log.Info("====== Shift Get List ======", logger.Any("req", req))

	resp, err := u.strg.Shift().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Shift Get List: u.strg.Shift().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ShiftService) Update(ctx context.Context, req *sale_service.ShiftUpdate) (*sale_service.Shift, error) {
	u.log.Info("====== Product Update ======", logger.Any("req", req))

	resp, err := u.strg.Shift().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Shift Update: u.strg.Shift().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if resp.Status == "closed" {
		u.log.Error("kassa Successfully closed", logger.Error(err))

	}

	return resp, nil
}

func (u *ShiftService) Delete(ctx context.Context, req *sale_service.ShiftPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Shift Delete ======", logger.Any("req", req))

	err := u.strg.Shift().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Shift Delete: u.strg.Shift().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
