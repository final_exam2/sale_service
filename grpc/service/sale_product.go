package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/final_exam2/sale_service/config"
	"gitlab.com/final_exam2/sale_service/genproto/sale_service"
	"gitlab.com/final_exam2/sale_service/grpc/client"
	"gitlab.com/final_exam2/sale_service/pkg/logger"
	"gitlab.com/final_exam2/sale_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type SaleProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*sale_service.UnimplementedSaleProductServiceServer
}

func NewSaleProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *SaleProductService {
	return &SaleProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *SaleProductService) Create(ctx context.Context, req *sale_service.SaleProductCreate) (*sale_service.SaleProduct, error) {
	u.log.Info("====== SaleProduct Create ======", logger.Any("req", req))

	resp, err := u.strg.SaleProduct().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create SaleProduct: u.strg.SaleProduct().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SaleProductService) GetById(ctx context.Context, req *sale_service.SaleProductPrimaryKey) (*sale_service.SaleProduct, error) {
	u.log.Info("====== SaleProduct Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.SaleProduct().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While SaleProduct Get By ID: u.strg.SaleProduct().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SaleProductService) GetList(ctx context.Context, req *sale_service.SaleProductGetListRequest) (*sale_service.SaleProductGetListResponse, error) {
	u.log.Info("====== SaleProduct Get List ======", logger.Any("req", req))

	resp, err := u.strg.SaleProduct().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While SaleProduct Get List: u.strg.SaleProduct().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SaleProductService) Update(ctx context.Context, req *sale_service.SaleProductUpdate) (*sale_service.SaleProduct, error) {
	u.log.Info("====== Product Update ======", logger.Any("req", req))

	resp, err := u.strg.SaleProduct().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While SaleProduct Update: u.strg.SaleProduct().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SaleProductService) Delete(ctx context.Context, req *sale_service.SaleProductPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== SaleProduct Delete ======", logger.Any("req", req))

	err := u.strg.SaleProduct().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While SaleProduct Delete: u.strg.SaleProduct().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
