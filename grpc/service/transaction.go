package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/final_exam2/sale_service/config"
	"gitlab.com/final_exam2/sale_service/genproto/sale_service"
	"gitlab.com/final_exam2/sale_service/grpc/client"
	"gitlab.com/final_exam2/sale_service/pkg/logger"
	"gitlab.com/final_exam2/sale_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type TransactionService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*sale_service.UnimplementedTransactionServiceServer
}

func NewTransactionService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *TransactionService {
	return &TransactionService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *TransactionService) Create(ctx context.Context, req *sale_service.TransactionCreate) (*sale_service.Transaction, error) {
	u.log.Info("====== Transaction Create ======", logger.Any("req", req))

	resp, err := u.strg.Transaction().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Transaction: u.strg.Transaction().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *TransactionService) GetById(ctx context.Context, req *sale_service.TransactionPrimaryKey) (*sale_service.Transaction, error) {
	u.log.Info("====== Transaction Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Transaction().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Transaction Get By ID: u.strg.Transaction().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *TransactionService) GetList(ctx context.Context, req *sale_service.TransactionGetListRequest) (*sale_service.TransactionGetListResponse, error) {
	u.log.Info("====== Transaction Get List ======", logger.Any("req", req))

	resp, err := u.strg.Transaction().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Transaction Get List: u.strg.Transaction().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *TransactionService) Update(ctx context.Context, req *sale_service.TransactionUpdate) (*sale_service.Transaction, error) {
	u.log.Info("====== Product Update ======", logger.Any("req", req))

	resp, err := u.strg.Transaction().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Transaction Update: u.strg.Transaction().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *TransactionService) Delete(ctx context.Context, req *sale_service.TransactionPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Transaction Delete ======", logger.Any("req", req))

	err := u.strg.Transaction().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Transaction Delete: u.strg.Transaction().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
